import base64 from 'react-native-base64'
import {SPOTIFY_AUTH, SPOTIFY_CATEGORIES_URL, SPOTIFY_ITEMS_BY_CATEGORIES_URL} from '../constants/endpoints';

const base64_credentials = base64.encode(SPOTIFY_AUTH.CLIENT_ID+':'+SPOTIFY_AUTH.CLIENT_SECRET)

export const getToken = () => {

    return fetch(SPOTIFY_AUTH.URL, {
        method: 'POST',
        headers: {
            'authorization':'Basic ' + base64_credentials,
            'content-type': 'application/x-www-form-urlencoded'
        },
        body: 'grant_type=client_credentials'
      })
      .then((response) => response.json());

  };

  // Consulta nuevos lanzamientos de Spotify
  export const getCategories = (token) => {
    return fetch(SPOTIFY_CATEGORIES_URL, {
        method: 'GET',
        headers: {
            'authorization':'Bearer  '+token
        }
      })
      .then((response) => response.json());
  };

 //Consulta items de una playlist
  export const getTracks = (idCategory, token) => {
    return fetch(SPOTIFY_ITEMS_BY_CATEGORIES_URL+idCategory, {
        method: 'GET',
        headers: {
            'authorization':'Bearer  '+token
        }
      })
      .then((response) => response.json());
  };