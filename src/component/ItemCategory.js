import React from 'react';
import { StyleSheet, TouchableOpacity, Text, Image, View } from 'react-native';
import {navigate} from '../RootNavigation';



const ItemCategory = (props) => {

    const {id, title, artist, poster_url, width, colorTitle, colorArtist} = props;
    
    return (
      <View style={styles.container}>
      <TouchableOpacity onPress={()=>{navigate('DetailCategoryView',{idCategory: id ,title: title })}} activeOpacity={0.8}>
        <Image
          style={[styles.imgPoster,{width:width}]}
          defaultSource={require('../assets/placeholder.png')}
          source={{ uri: poster_url }}
        />
        <Text style={[styles.textTitle,{color:colorTitle}]} numberOfLines={1} ellipsizeMode="tail">
          {title}
        </Text>
        <Text style={[styles.textArtist,{color:colorArtist}]} numberOfLines={1} ellipsizeMode="tail">
          {artist}
        </Text>
      </TouchableOpacity>
      </View>
    );
  };

  const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        justifyContent: "center",
        alignItems:'center',
        backgroundColor: '#000',
        borderWidth:3,
        borderColor:'#000',
        paddingRight:0,
        paddingLeft:0
    },
    textTitle:{
      fontSize:14
    },
    textArtist:{
      fontSize:10
    },
    imgPoster: {
      height: 200,
      alignSelf: 'stretch',
      borderWidth:2,
      borderColor:'#000',
    },
  });

  export default ItemCategory;