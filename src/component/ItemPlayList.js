import React from 'react';
import { StyleSheet, TouchableOpacity, Text, Image, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLOR} from '../constants/colors';


const ItemPlayList = (props) => {

    const { id, title, duration, preview_url } = props;


    const millisToMinutesAndSeconds = (millis) => {
        let minutes = Math.floor(millis / 60000); 
        let seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    return(
        <View style={styles.container}>
            <View style={{flex:1}}>
            <TouchableOpacity activeOpacity={0.5}>
                <Icon name="play-circle-outline" size={45} color={COLOR.TEXT_ICONS} />
            </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
                <Text style={[styles.textTitle]} numberOfLines={1} ellipsizeMode="tail">
                    {title}
                </Text>
            </View>
            <View style={{flex:1}}>
                <Text style={[styles.textDuration]}>
                    {millisToMinutesAndSeconds(duration)}
                </Text>
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems:'center',
        backgroundColor: COLOR.ACCENT_COLOR,
        borderBottomWidth:0.5,
        borderBottomColor:"#ccc",
        paddingRight:0,
        paddingLeft:0,
        height:65,
    },
    textTitle:{
        fontSize:16,
        color:COLOR.TEXT_ICONS,
        paddingLeft:0
    },
    textDuration:{
        fontSize:15,
        color:COLOR.SECONDARY_TEXT,
        paddingRight:10
    }
  });

export default ItemPlayList;