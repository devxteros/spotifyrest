/* 
Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences.
COLOR TOOL
Create, share, and apply color palettes to your UI, as well as measure the accessibility level of any color combination.
Visit: https://material.io/color
*/
export const COLOR = {
    PRIMARY_DARK:'#008600',
    PRIMARY_LIGHT:'#7eea4e',
    PRIMARY:'#00aa00',
    TEXT_ICONS:'#FFFFFF',
    ACCENT_COLOR:'#000000',
	PRIMARY_TEXT:'#FFFFFF',
    SECONDARY_TEXT:'#757575',
    DIVIDER_COLOR: '#BDBDBD',
}