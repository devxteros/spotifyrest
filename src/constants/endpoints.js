//Endpoints para captura de datos
export const API_MOCKABLE = {
	LOGIN : 'https://demo5108894.mockable.io/',
}

export const SPOTIFY_AUTH = {
	URL : 'https://accounts.spotify.com/api/token',
	CLIENT_ID: '1479c85f4c9e414e9a0f4813e78a6372',
	CLIENT_SECRET: 'bd08d8a5942c421a9d7332e369271703',
}

export const SPOTIFY_CATEGORIES_URL = 'https://api.spotify.com/v1/browse/new-releases?country=CO&limit=50';

export const SPOTIFY_ITEMS_BY_CATEGORIES_URL = 'https://api.spotify.com/v1/albums/';