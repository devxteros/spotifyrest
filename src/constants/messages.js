export const LABELS = {
	TITLE : 'Spotify REST',
    CATEGORIES : 'GENEROS MUSICALES',
    POPULARS : 'LAS CANCIONES MÁS POPULARES',
}

//Texto titulos de los mensajes tipo Alert
export const ALERTS = {
	ERROR : 'Error',
    WARNING : 'Advertencia!',
    INFO : 'Información',
}

//Textos pantalla Login 
export const LOGIN_MSG = {
	TITLE : 'SPOTIFY REST',
    USERNAME : 'USUARIO',
    PASSWORD : 'CONTRASEÑA',
    LOGIN: 'INGRESAR',
    ONLOGIN : 'INGRESANDO...',
    EMPTY_FIELDS:'ESCRIBA USUARIO Y CONTRASEÑA',
    INVALID_AUTH: 'USUARIO/CONTRASEÑA NO VÁLIDOS'
}

export const CATEGORIES_MSG = {
    ISLOADING : 'CARGANDO PLAYLIST...',
    TITLE:'Nuevos Lanzamientos',
}

export const DETAIL_CATEGORY_MSG = {
    ISLOADING : 'CARGANDO PISTAS...',
}

