import React, {useState, useEffect} from 'react';
import {SafeAreaView,StyleSheet,useWindowDimensions,View,Text,ActivityIndicator,VirtualizedList,TouchableOpacity} from 'react-native';
import {getToken, getCategories} from '../api/spotify';
import {CATEGORIES_MSG} from '../constants/messages';
import {COLOR} from '../constants/colors';
import ItemCategory from '../component/ItemCategory';



function CategoriesView() {

  const[categories, setCategories] = useState([]);
  const[isLoading, setIsLoading] = useState(true);
  const window = useWindowDimensions();


  useEffect(() => {
    
    getToken()
    .then((json)=>{
      global.token = json.access_token; //Se guarda el token para tenerlo disponible en la pantalla detalle
      getCategories(json.access_token)
      .then((json)=>{setCategories(json.albums.items)})
    })
    .catch((error) => {
      console.log(error);
    })
    .finally(() => {setIsLoading(false)});
    
  },[]);


  const getItem = (data, index) => {

    let items = [];
    for (let i = 0; i < 2; i++) {
      const item = data[index*2+i];
      item && items.push(item)
    }
    return items;

  }

  const getItemCount = (data) => {
    return data.length;
  }

  const handleClick = () => {
     console.log('Se hizo click');
 }

  if (isLoading == true) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#00ff00" />
        <Text style={styles.textLoading}>{CATEGORIES_MSG.ISLOADING}</Text>
      </View>
    )
  }

  return (
    <View style={{backgroundColor:"#000"}}>
      <VirtualizedList
        style={{backgroundColor:"#000"}}
        data={categories}
        keyExtractor={(item, index) => index.toString()}
        getItemCount={getItemCount}
        getItem={getItem}
        renderItem={({item, index}) => {
          return (
            <View key={index} style={{flexDirection: 'row'}}>
              {item.map((elem, i) => (
                  <ItemCategory 
                    key={elem.id}
                    id={elem.id}
                    title={elem.name}
                    artist={elem.artists[0].name}
                    poster_url={elem.images[0].url}
                    width={window.width/2}
                    colorTitle={COLOR.TEXT_ICONS}
                    colorArtist={COLOR.PRIMARY}
                  />
              ))}
            </View>
          )
        }}
      />
      
      </View>
  );
};

const styles = StyleSheet.create({
  container:{
      flex:1,
      justifyContent: "center",
      alignItems:'center',
      backgroundColor: COLOR.ACCENT_COLOR,
  },
  textLoading:{
    color:'#fff',
  }
});

export default CategoriesView;