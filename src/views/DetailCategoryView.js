import React, {useState, useEffect} from 'react';
import { StyleSheet, SafeAreaView, View, Text, VirtualizedList,ActivityIndicator } from 'react-native';
import ItemPlayList from '../component/ItemPlayList';
import {COLOR} from '../constants/colors';
import { getTracks } from '../api/spotify';
import {DETAIL_CATEGORY_MSG} from '../constants/messages';

function DetailCategoryView({route}) {

  const[tracks, setTracks] = useState([]);
  const[isLoading, setIsLoading] = useState(true);
  const { idCategory } = route.params;


  useEffect(() => {
    
    getTracks(idCategory, global.token)
    .then((json)=>{
      //console.log(json.tracks.items);
      setTracks(json.tracks.items);
    })
    .catch((error) => {
      console.log(error);
    })
    .finally(() => {setIsLoading(false)});
    
  },[]);

  const getItem = (data, index) => {
    return data[index];
  }

  const getItemCount = (data) => {
    return data.length;
  }


  if (isLoading == true) {
    return (
      <View style={styles.containerLoad}>
        <ActivityIndicator size="large" color="#00ff00" />
        <Text style={styles.textLoading}>{DETAIL_CATEGORY_MSG.ISLOADING}</Text>
      </View>
    )
  }

  return (
          <SafeAreaView style={styles.container}>
            
            <VirtualizedList
              data={tracks}              
              keyExtractor={item => item.id}
              getItemCount={getItemCount}
              getItem={getItem}
              renderItem={({ item }) => {
                return(
                  <ItemPlayList
                      id={item.id} 
                      title={item.name}
                      duration={item.duration_ms}
                      preview_url={item.preview_url}
                  />
                )
              }}
            />
          </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor: COLOR.ACCENT_COLOR,
  },
  containerLoad:{
    flex:1,
    backgroundColor: COLOR.ACCENT_COLOR,
    justifyContent: "center",
    alignItems:'center',
},
  textLoading:{
    color:'#fff',
  }
});

export default DetailCategoryView;