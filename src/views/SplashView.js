import React,{ useEffect } from 'react';
import { StyleSheet, View, ImageBackground, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';



function SplashView({navigation}) {

  //Valida usuario con la "sesion" abierta.
  const getLoggedUser = async () => {
  
    try {
      const value = await AsyncStorage.getItem('@logged_user');
      console.log(value);
      if(value !== null) {

          navigation.reset({
            index: 0,
            routes: [{ name: 'CategoriesView' }] //Vista Categorias
          });

      }else{

          navigation.reset({
            index: 0,
            routes: [{ name: 'LoginView' }] //Vista Login 
          });

      }
    } catch(e) {
      console.log(e);
    }
  }

  //Espera de 3 segundos... mientras mostrar pantalla Splash
  useEffect(() => {
    const timer = setTimeout(() => {
      getLoggedUser();
    }, 3000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
      <ActivityIndicator size="large" color="#00ff00" />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container:{
      flex:1
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
});

export default SplashView;