import React, { useState, useEffect, useRef } from 'react';
import {StyleSheet,View, TextInput, Button, ImageBackground, Image, ToastAndroid, Dimensions, Platform, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {LOGIN_MSG, ALERTS} from '../constants/messages';
import {API_MOCKABLE} from '../constants/endpoints';
import {COLOR} from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';

const win = Dimensions.get('window');

function LoginView({ navigation }) {
  const [username, setUsername] = useState('');//demo
  const [password, setPassword] = useState('');//1234
  const [isLogin, setIsLogin] = useState(false);
  const passwordRef = useRef(null);

  const setLoggedUser = async (value) => {
    try {
      await AsyncStorage.setItem('@logged_user', value)
    } catch (e) {
      
      if (Platform.OS === 'android') {
        ToastAndroid.show(e, ToastAndroid.LONG);
      }else{
        Alert.alert('Ops!', e);
      }

    }
  }

  const onLogin = () => {
    if(username == '' || password == ''){ //Valida campos vacios
      
      if (Platform.OS === 'android') {
        ToastAndroid.show(LOGIN_MSG.EMPTY_FIELDS, ToastAndroid.SHORT);
      } else {
        Alert.alert(ALERTS.WARNING, LOGIN_MSG.EMPTY_FIELDS);
      }
    }else{
      setIsLogin(true);
      //Emulando una autenticación https://demo5108894.mockable.io/demo/1234 mediante POST
      fetch(API_MOCKABLE.LOGIN+username+'/'+password, {
          method: 'POST'
      })
      .then((response) => response.json())
      .then((json) => {
          
          const user_string = JSON.stringify(json.user);
          setLoggedUser(user_string);   //Guarda información del usuario emulando una "sesion"

          // Redirecciona a la pantalla de categorias
          navigation.reset({
            index: 0,
            routes: [{ name: 'CategoriesView' }]
          });

      })
      .catch((error) => {
        setIsLogin(false);
        if (Platform.OS === 'android') {
          ToastAndroid.show(LOGIN_MSG.INVALID_AUTH, ToastAndroid.LONG);
        } else {
          Alert.alert(ALERTS.ERROR,LOGIN_MSG.INVALID_AUTH);
        }
      })
      .finally(() => {console.log('finally')});
    }
  }


  return(
    
    <ImageBackground source={require('../assets/login.png')} style={styles.image}>
      <Image resizeMethod={'scale'} source={require('../assets/logo.png')} style={styles.logo}/>
      <View style={styles.container}>

          <View style={styles.inputTextLayer}>
            <View style={styles.inputIcon}>
              <Icon name="account" size={30} color={COLOR.TEXT_ICONS} />
            </View>
            <View style={{flex:1}}>
              <TextInput
                returnKeyType='next'
                onSubmitEditing={() => passwordRef.current.focus()}
                keyboardType='default'
                style={styles.inputText}
                onChangeText={u => setUsername(u)}
                value={username}
                underlineColorAndroid={'transparent'}
                placeholder={LOGIN_MSG.USERNAME}
                placeholderTextColor={COLOR.PRIMARY}
                maxLength={50}
              />
            </View>
          </View>

          <View style={styles.inputTextLayer}>
            <View style={styles.inputIcon}>
              <Icon name="lock" size={30} color={COLOR.TEXT_ICONS} />
            </View>
            <View style={{flex:1}}>
            <TextInput
              ref={passwordRef}
              keyboardType='default'
              style={styles.inputText}
              onChangeText={p => setPassword(p)}
              value={password}
              underlineColorAndroid={'transparent'}
              secureTextEntry={true}
              placeholder={LOGIN_MSG.PASSWORD}
              placeholderTextColor={COLOR.PRIMARY}
              maxLength={50}
            />
            </View>
          </View>

          <View style={{flex:1, paddingTop:60}}>
            <Button
              onPress={onLogin}
              title={(isLogin) ? LOGIN_MSG.ONLOGIN : LOGIN_MSG.LOGIN}
              color={COLOR.PRIMARY}
            />
          </View>
          
      </View>
      </ImageBackground>
    
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    marginHorizontal: 30,
  }, 
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center"
  },
  logo:{
    flex:1,
    width: win.width,
    resizeMode:"center"
  },
  inputTextLayer:{    
    flexDirection:'row',
  },
  inputText:{    
    borderLeftWidth:0,
    borderRightWidth:0,
    borderTopWidth:0,
    borderBottomWidth:1,
    borderBottomColor:COLOR.TEXT_ICONS,  
    color:COLOR.TEXT_ICONS  
  },
  inputIcon:{
    borderBottomWidth:1,
    borderBottomColor:COLOR.TEXT_ICONS,
    alignItems:'center',
    justifyContent:'center'
  },
});

export default LoginView;