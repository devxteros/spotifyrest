import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {LABELS,CATEGORIES_MSG} from './constants/messages';
import {COLOR} from './constants/colors';

/* vistas android */
import SplashView from './views/SplashView';
import LoginView from './views/LoginView';
import CategoriesView from './views/CategoriesView';
import DetailCategoryView from './views/DetailCategoryView';
import {navigationRef} from './RootNavigation';

const Stack = createStackNavigator();


function App() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
      initialRouteName='SplashView'>
        <Stack.Screen name="SplashView" component={SplashView} options={{ headerShown: false}} />
        <Stack.Screen name="LoginView" component={LoginView} options={{ headerShown: false}} />
        <Stack.Screen name="CategoriesView" component={CategoriesView} options={{ title: CATEGORIES_MSG.TITLE }} />
        <Stack.Screen name="DetailCategoryView" component={DetailCategoryView} options={({ route }) => ({idCategory: route.params.idCategory ,title: route.params.title })}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#eee",
  },
  container:{
      flex:1
  }
});

export default App;