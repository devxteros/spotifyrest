# Spotify News

Aplicación móvil experimental para Android y iOS, construida en [React Native 0.63](https://reactnative.dev/docs/getting-started) usando componentes funcionales y Hooks. La aplicación se conecta al servicio RestFull de Spotify para consumir los nuevos lanzamientos musicales.

Datos de acceso:

- **Usuario:** _demo_
- **Contraseña:** _1234_

Android

![Spotify News Android](https://bitbucket.org/devxteros/spotifyrest/raw/50caf49164c35a0ae9816a55806482eee355b190/captura.gif)

iOS

![Spotify News Android](https://bitbucket.org/devxteros/spotifyrest/raw/94638da7c8b999438c62ebba685311e950528188/captura-ios.gif)

## Instalación

Clonar el repositorio

```bash
git clone https://devxteros@bitbucket.org/devxteros/spotifyrest.git
```

Ingrese en la carpeta raíz del proyecto y ejecute el siguiente comando para instalar dependencias

```bash
npm install
```

En la construcción se usaron las siguientes dependencias:

- **@react-native-async-storage/async-storage**
- **@react-navigation/native**
- **react-native-base64**
- **react-native-vector-icons**

## Ejecutar la aplicación

Ingresar al directorio raíz del proyecto y ejecute el comando:

```bash
react-native run-start
```

### Compilación

En una ventana bash del sistema ubiquese en el directorio raíz del proyecto y ejecute el comando de compilacion segun el sistema operativo de su telefono:

### Compilar para Android

```bash
react-native run-android
```

### Compilar para iOS

```bash
react-native run-ios
```

### Versiones de Software

- Node v12.11.1
- Java 1.8.0_192
- Xcode 12.3
- React Native 0.63
